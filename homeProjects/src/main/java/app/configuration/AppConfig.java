package app.configuration;

import app.service.FirstService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "app")
public class AppConfig {


    @Bean(name = "firstService")
    public FirstService firstService() {
        return new FirstService("Mico");
    }

}
