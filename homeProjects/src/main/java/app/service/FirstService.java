package app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FirstService {

    private final String name;

    @Autowired
    private SecondService secondService;

    public FirstService(String mico) {
        this.name = mico;
    }

    public String doSomething(int x, int y) {
        System.out.println("Primul meu service instantiat prin DI, pe nume:" + name);
        int result = secondService.doSomth(x, y);
        return "" + result;
    }

}
