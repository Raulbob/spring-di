package app.service;

import org.springframework.stereotype.Service;

@Service
public class SecondService {

    public int doSomth(int x, int y) {
        return x*y;
    }

}
