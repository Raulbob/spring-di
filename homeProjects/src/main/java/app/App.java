package app;

import app.configuration.AppConfig;
import app.service.FirstService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class App {



    public static void main(String[] args) {
        System.out.println("Salutare Raul. Eu sunt prima ta aplicatie Maven. Tu esti creatorul meu. Multumesc");
        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
        FirstService  obj = (FirstService) ctx.getBean("firstService");
        String r = obj.doSomething(13,13);
        System.err.println(r);
    }

}
